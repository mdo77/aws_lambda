﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ascribe.Lambda.VTT
{
  public class AscribeEnvironmentInfo
  {
    public string url { get; set; }
    public string userName { get; set; }
    public string password { get; set; }

  }
  public class QuestionConfig
  {
    public string serviceName { get; set; }
    public string modelName { get; set; }
    public string userName { get; set; }
    public string password { get; set; }

  }
  public class AscribeSessionRequest
  {
    public string account { get; set; }
    public string userName { get; set; }
    public string password { get; set; }
  }
  public abstract class BaseResponse
  {
    public string[] errors { get; set; }
  }
  public class AscribeSessionResponse : BaseResponse
  {
    public string authenticationToken { get; set; }
  }
  public class AscribePutResponseRequest
  {
    public List<AscribeResponse> responses { get; set; }
  }
  public class AscribePutResponseResponse : BaseResponse
  {
    public int responsesAdded { get; set; }
    public int existingResponsesUpdated { get; set; }
    public int existingResponsesUnchanged { get; set; }
  }
  public class AscribeResponse
  {
    public string rid { get; set; }
    public string text { get; set; }
  }
}
