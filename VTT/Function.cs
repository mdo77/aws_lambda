﻿using IBM.WatsonDeveloperCloud.SpeechToText.v1;
using IBM.WatsonDeveloperCloud.SpeechToText.v1.Model;
using IBM.WatsonDeveloperCloud.SpeechToText.v1.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Amazon.Lambda.S3Events;
using Amazon.S3;
using Amazon.S3.Util;
using System.Text;
using System.Net.Http;
using RestSharp;
using System.IO;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Ascribe.Lambda.VTT
{
  public static class RestClientExtensions
  {
    public static async Task<RestResponse> ExecuteAsync(this RestClient client, RestRequest request)
    {
      TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();
      RestRequestAsyncHandle handle = client.ExecuteAsync(request, r => taskCompletion.SetResult(r));
      return (RestResponse)(await taskCompletion.Task);
    }
  }
  public class Function
  {
    IAmazonS3 S3Client { get; set; }

    /// <summary>
    /// Default constructor. This constructor is used by Lambda to construct the instance. When invoked in a Lambda environment
    /// the AWS credentials will come from the IAM role associated with the function and the AWS region will be set to the
    /// region the Lambda function is executed in.
    /// </summary>
    public Function()
    {
      S3Client = new AmazonS3Client();
    }

    /// <summary>
    /// Constructs an instance with a preconfigured S3 client. This can be used for testing the outside of the Lambda environment.
    /// </summary>
    /// <param name="s3Client"></param>
    public Function(IAmazonS3 s3Client)
    {
      this.S3Client = s3Client;
    }

    /// <summary>
    /// This method is called for every Lambda invocation. This method takes in an S3 event object and can be used 
    /// to respond to S3 notifications.
    /// </summary>
    /// <param name="evnt"></param>
    /// <param name="context"></param>
    /// <returns></returns>
    public async Task<string> FunctionHandler(S3Event evnt, ILambdaContext context)
    {
      var s3Event = evnt.Records?[0].S3;
      string retVal = string.Empty;
      if (s3Event == null)
      {
        return null;
      }

      try
      {
        var response = await this.S3Client.GetObjectMetadataAsync(s3Event.Bucket.Name, s3Event.Object.Key);
        string transcription = "";

        string[] info = s3Event.Object.Key.Split('/');
        string ext = System.IO.Path.GetExtension(s3Event.Object.Key);
        
        // don't process json files.
        if (ext.ToLower() == "json") return string.Empty;
        string ascribe_environment = info[0];
        string account = info[1];
        string question = info[2];
        string respondent = System.IO.Path.GetFileNameWithoutExtension(info[3]);

        var questionConfig = await GetObjectFromS3File<QuestionConfig>(s3Event.Bucket.Name, string.Format("{0}/{1}/{2}/q.json", ascribe_environment, account, question));

        switch (questionConfig.serviceName  )
        {
          case "IBM":
            if (response.Headers.ContentType.ToLower().Contains("wav"))
            {
              transcription = Transcribe_IBM(s3Event, questionConfig).Result;//DateTime.Now.ToString(); //
            }
            break;
          case "test":
            transcription = DateTime.UtcNow.ToString();
            break;
          default:
            transcription = string.Format("Invalid Transcription Processor [{0}]", questionConfig.serviceName);
            break;
        }

        // get environment info
        var envInfo = await GetObjectFromS3File<AscribeEnvironmentInfo>(s3Event.Bucket.Name, string.Format("{0}/{0}.json", ascribe_environment));
        var restClient = new RestClient(envInfo.url);

        string ascribeAuthGuid = await AscribeAuth(s3Event, restClient, account, envInfo);
        AscribePutResponseResponse putResponse = await AddResponse(s3Event, restClient, ascribeAuthGuid, transcription, respondent, question);
        return string.Format("Account: {0}; Question: {1}; Respondent: {2}; Token: {3}; Responses Added: {4}; Responses Updated: {5}", account, question, respondent, ascribeAuthGuid, putResponse.responsesAdded, putResponse.existingResponsesUpdated);

      }
      catch (Exception e)
      {
        context.Logger.LogLine($"Error getting object {s3Event.Object.Key} from bucket {s3Event.Bucket.Name}. Make sure they exist and your bucket is in the same region as this function.");
        context.Logger.LogLine(e.Message);
        context.Logger.LogLine(e.StackTrace);
        throw;
      }
    }


    internal async Task<T> GetObjectFromS3File<T>(string BucketName, string ObjectKey)
    {
      string json = string.Empty;
      using (var stream = await this.S3Client.GetObjectStreamAsync(BucketName, ObjectKey, null))
      {
        using (TextReader reader = new StreamReader(stream))
        {
          json = reader.ReadToEnd();
        }
      }
      return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
    }

    protected async Task<string> Transcribe_IBM(S3EventNotification.S3Entity s3Event, QuestionConfig questionConfig)
    {
      
      string mediaType = "audio/wav";
      var stream = await this.S3Client.GetObjectStreamAsync(s3Event.Bucket.Name, s3Event.Object.Key, null);
      SpeechToTextService s2t = new SpeechToTextService();

      // load credentials for IBM service from "IBM/creds.json"
      s2t.SetCredential(questionConfig.userName, questionConfig .password);

      var session = s2t.CreateSession(questionConfig.modelName);// "");

      var metaData = new IBM.WatsonDeveloperCloud.SpeechToText.v1.Model.Metadata() { PartContentType = mediaType };// response.Headers.ContentType };
      metaData.SmartFormatting = true;
      SpeechRecognitionEvent speechEvent = null;
      speechEvent = s2t.RecognizeWithSession(session.SessionId, mediaType, metaData, stream);

      StringBuilder sb = new StringBuilder();
      foreach (var result in speechEvent.Results)
      {
        string s = result.Alternatives[0].Transcript.Replace("%HESITATION", "").Trim();
        if (s.Length == 0) continue;
        s = string.Format("{0}{1}. ", s.Substring(0, 1).ToUpper(), s.Substring(1));
        sb.Append(s);
      }

      return sb.ToString().Trim();
    }
    protected async Task<string> AscribeAuth(S3EventNotification.S3Entity s3Event, RestClient client, string account, AscribeEnvironmentInfo envInfo)
    {
      var request = new RestRequest("Sessions", Method.POST);
      request.AddHeader("cache-control", "no-cache");
      request.AddHeader("content-type", "application/json");
      request.AddHeader("accept", "application/json, text/json, application/xml, text/xml");
      var sessionRequest = new AscribeSessionRequest() { account = account, password = envInfo.password, userName = envInfo.userName };
      request.AddJsonBody(sessionRequest);
      IRestResponse restResponse = await client.ExecuteAsync(request);
      AscribeSessionResponse sessionResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<AscribeSessionResponse>(restResponse.Content);
      return sessionResponse.authenticationToken;
    }

    protected async Task<AscribePutResponseResponse> AddResponse(S3EventNotification.S3Entity s3Event, RestClient client, string authGuid, string Transcription, string respondent, string question)
    {
      var request = new RestRequest("Responses/" + question.ToString(), Method.PUT);
      request.AddHeader("cache-control", "no-cache");
      request.AddHeader("content-type", "application/json");
      request.AddHeader("accept", "application/json, text/json, application/xml, text/xml");
      request.AddHeader("authentication", authGuid);
      var responseRequest = new AscribePutResponseRequest()
      {
        responses = new List<AscribeResponse>(new AscribeResponse[] {
          new AscribeResponse() { rid = respondent, text = Transcription }
        })
      };
      request.AddJsonBody(responseRequest);
      var restResponse = await client.ExecuteAsync(request);
      AscribePutResponseResponse putResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<AscribePutResponseResponse>(restResponse.Content);
      return putResponse;
    }
   
  }
}
